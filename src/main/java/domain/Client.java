package domain;

import java.time.LocalDate;
import java.util.Date;

public class Client {

    public Client(String name, String firstName, LocalDate birthDate, Date crationDate) {
        this.name = name;
        this.firstName = firstName;
        this.birthDate = birthDate;
        this.crationDate = crationDate;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public LocalDate getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(LocalDate birthDate) {
        this.birthDate = birthDate;
    }

    public Date getCrationDate() {
        return crationDate;
    }

    public void setCrationDate(Date crationDate) {
        this.crationDate = crationDate;
    }

    private String name;
    private String firstName;
    private LocalDate birthDate;
    private Date crationDate;
}
